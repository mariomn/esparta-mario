//Processo Seletivo Esparta.io
//Candidato: Mario Morais Neto
//17 de Julho de 2020

export {};

const array: Array<number> = [2, 1, 1, 2, 3, 1];

function Solution(array: Array<number>): number {
    let distintos: Array<number> = [];

    array.forEach(item => { //percorrendo o array passado para a função e adicionando numeros distintos a um novo array
        if(distintos.indexOf(item) === -1){
            distintos.push(item);
        }
    })

    console.log(distintos.sort((a, b) => a - b));
    return distintos.length;
};

console.log('Solução: ', Solution(array));