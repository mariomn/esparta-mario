//Processo Seletivo Esparta.io
//Candidato: Mario Morais Neto
//17 de Julho de 2020

export {};

const array: Array<number> =[9, 3, 9, 3, 9, 7, 9];

function Solution(array:Array<number>):number{

    interface Elementos {
        numeros: Array<number>,
        ocorrencias: Array<number>,
    };

    let elementos: Elementos = { //objeto com os numeros e quantas vezes foram encontrados no array passado à função
        numeros: [],
        ocorrencias: []        
    };
    
    array.forEach(item => { //percorrendo o array passado à função
        const indexItem = elementos.numeros.indexOf(item);

        if(indexItem === -1){ //item ainda não existe
            elementos.numeros.push(item);
            elementos.ocorrencias.push(1);
        } 
        else{ //item já existe, aumenta a ocorrencia
            elementos.ocorrencias[indexItem] = elementos.ocorrencias[indexItem] + 1;
        }
    });
    
    let elementoSozinho = -1; //percorrendo o array de ocorrencias para identificar o numero com ocorrencias ímpares
    elementos.ocorrencias.forEach((item, i) => {
        if((item%2) === 1){
            elementoSozinho = elementos.numeros[i];
        }
    });

    console.log(elementos);
    return elementoSozinho;
}

console.log('Solução: ', Solution(array));