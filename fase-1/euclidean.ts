//Processo Seletivo Esparta.io
//Candidato: Mario Morais Neto
//17 de Julho de 2020

export {};

const n: number = 10; //tamanho
const m: number = 4; //incremento

function Solution(n: number, m: number): number {
    let chocolates: Array<Number> = new Array(n);
    chocolates.fill(1); //chocolates que ainda não foram comidos são representados por '1'

    let posicao: number = 0;
    let totalChocolates: number = 0;

    while(chocolates[posicao] !== 0){ //enquanto não encontrou uma embalagem vazia
        chocolates[posicao] = 0;
        posicao = (posicao + m) % n;
        totalChocolates++;
    }

    console.log(chocolates);
    return totalChocolates;
}

console.log('Solução: ', Solution(n, m));