Features do todoApp:  
-> Adicionar tarefa com nome e descrição desta (a tarefa só é criada depois de informar os dois).  
-> Botão '+' e TextInput do item de adicionar tarefa acionam o input de texto, ao confirmar o primeiro campo o segundo entra em foco automaticamente.  
-> Após criada uma tarefa, seu nome e descrição ainda podem ser editados (ambos ainda são TextInput).  
-> Header das tarefas concluídas só aparece se alguma delas existe, e mostra quantas são no seu título.  
-> Header das tarefas concluídas funciona como um botão para definir se elas devem ser exibidas ou não (por padrão não são exibidas).  
-> É possível remover tarefas concluídas e não concluídas apertando no botão 'X' à sua direita, que traz um alerta para confirmação, perguntando se o usuário tem certeza e mostrando o nome da tarefa que será apagada.  