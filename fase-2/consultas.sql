--Buscar os nomes de todos os alunos que frequentam alguma turma do professor 'JOAO PEDRO'.
select aluno.nome from aluno 
inner join aluno_turma 
on aluno.id = aluno_turma.aluno_id
inner join turma
on aluno_turma.turma_id = turma.id
inner join professor
on turma.professor_id=professor.id
where professor.nome='JOAO PEDRO'

--Buscar os dias da semana que tenham aulas da disciplina 'MATEMATICA'.
select turma.dia_da_semana from turma
inner join disciplina
on turma.disciplina_id=disciplina.id
where disciplina.nome='MATEMATICA'

--Buscar todos os alunos que frequentem aulas de 'MATEMATICA' e também 'FISICA'.
select * from aluno
inner join aluno_turma
on aluno.id=aluno_turma.aluno_id
inner join turma
on aluno_turma.turma_id=turma.id
inner join disciplina
on turma.disciplina_id=disciplina.id
where disciplina.nome='MATEMATICA' and disciplina.nome='FISICA'

--Buscar as disciplinas que não tenham nenhuma turma.
select * from disciplina
left join turma
on disciplina.id=turma.disciplina_id
where turma.disciplina_id is null

--Buscar os alunos que frequentem aulas de 'MATEMATICA' exceto os que frequentem 'QUIMICA'.
select * from aluno
inner join aluno_turma
on aluno.id=aluno_turma.aluno_id
inner join turma
on aluno_turma.turma_id=turma.id
inner join disciplina
on turma.disciplina_id=disciplina.id
where disciplina.nome='MATEMATICA' and disciplina.nome<>'QUIMICA'