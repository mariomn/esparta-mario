const appColors = {
    //fundo
    backgroundGrey: '#252E42',
    statusBarGrey: '#1D2333',
    itemBackground: '#1f2738',

    //texto
    titleText: '#FFF',
    descriptionGrey: '#707070',

    //checkbox
    checkboxBodyGrey: '#4F5565',
    checkboxBorderGrey: '#31394D',
    addItemGrey: '#ACACAC',

    white: '#FFF',
};

export default appColors;
