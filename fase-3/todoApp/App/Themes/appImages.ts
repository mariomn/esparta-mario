const appImages = {
    plusIcon: require('../Images/plus.png'),
    removeIcon: require('../Images/x.png'),
    down: require('../Images/down.png'),
    up: require('../Images/up.png'),
};

export default appImages;