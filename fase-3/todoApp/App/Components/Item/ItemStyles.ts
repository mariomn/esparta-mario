import { TextStyle, ViewStyle, ImageStyle } from 'react-native';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import appColors from '../../Themes/appColors';

export default {
    backgroundView: {
        width: wp('80%'),
        marginTop: hp('2%'),
        paddingTop: hp('1%'),
        flexDirection: 'row',
        backgroundColor: appColors.itemBackground,
        borderRadius: wp('4%'),
    } as ViewStyle,

    checkbox: {
        //marginTop: ('1.5%'),
        marginLeft: wp('1%'),
    } as ViewStyle,

    textInputsContainer: {
        width: '70%',
        marginLeft: wp('3%'),
        marginRight: wp('3%'),
    } as ViewStyle,

    mainTextInput: {
        color: appColors.white,
        fontSize: wp('4%'),
        paddingTop: 0,
    } as TextStyle,

    secondaryTextInput: {
        color: appColors.addItemGrey,
        fontSize: wp('2.5%'),
        paddingTop: 0,
    } as TextStyle,

    mainTextChecked: {
        color: appColors.addItemGrey,
        fontSize: wp('4%'),
        marginBottom: hp('1.5%'),
        textDecorationLine: 'line-through',
        paddingTop: 0,
    } as TextStyle,

    secondaryTextChecked: {
        color: appColors.descriptionGrey,
        marginBottom: hp('1.5%'),
        textDecorationLine: 'line-through',
        fontSize: wp('2.5%'),
    } as TextStyle,

    removeTouchable: {
        marginTop: hp('2.5%'),
    } as ViewStyle,

    removeIcon: {
        height: wp('4%'),
        width: wp('4%'),
    } as ImageStyle,

};