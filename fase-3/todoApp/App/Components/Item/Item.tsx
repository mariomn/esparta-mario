import React from 'react';
import { View, Text, Image, TextInput, TouchableOpacity } from 'react-native';
import styles from './ItemStyles'
import CheckBox from '@react-native-community/checkbox';
import appImages from '../../Themes/appImages';
import appColors from '../../Themes/appColors';

//o item é mutável, pode ser do tipo checado ou ainda não checado
export enum ItemType {
    CHECKED,
    UNCHECKED,
}

export interface Props {
    itemType: ItemType,
    id: number,
    mainTextInitial: string,
    secondaryTextInitial: string,
    onCheck: (id:number, itemType:ItemType) => void,
    onRemove: (id:number, itemType:ItemType) => void,
    onSubmit?: (id:number, mainText:string, secondaryText:string) => void,
}

export interface State {
    checkboxState: boolean,
    mainText: string,
    secondaryText: string,
}

export class Item extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            checkboxState: this.props.itemType === ItemType.CHECKED ? true : false,
            mainText: this.props.mainTextInitial,
            secondaryText: this.props.secondaryTextInitial,
        };
    }

    async updateMainInput(text:string){
        await this.setState({
            mainText: text
        });
        this.inputsSubmitted();
    }

    async updateSecondaryInput(text:string){
        await this.setState({
            secondaryText: text
        });
        this.inputsSubmitted();
    }

    inputsSubmitted(){
        const { mainText, secondaryText } = this.state;
        const { id, onSubmit } = this.props;

        typeof onSubmit === 'function' ? onSubmit(id, mainText, secondaryText) : null;
    }

    handleCheckbox(){
        const { onCheck, id, itemType } = this.props;
        const { checkboxState } = this.state;

        checkboxState ? this.setState({ checkboxState: false }) : this.setState({ checkboxState: true });
        onCheck(id, itemType);
    }

    render() {
        const { checkboxState, mainText, secondaryText } = this.state;
        const { id, itemType, onRemove } = this.props;

        return (
            <View>
                {
                    itemType === ItemType.UNCHECKED ? 
                    (
                        <View style={styles.backgroundView}>
                            <CheckBox
                                tintColors={appColors.white}
                                style={styles.checkbox}
                                disabled={false}
                                value={checkboxState}
                                onValueChange={() => this.handleCheckbox()}
                            />

                            <View style={styles.textInputsContainer}> 
                                <TextInput
                                    value={mainText}
                                    style={styles.mainTextInput}
                                    selectionColor={appColors.addItemGrey}
                                    multiline= {true}
                                    blurOnSubmit = {true}
                                    onChangeText= {text => this.updateMainInput(text)}
                                >
                                </TextInput>

                                <TextInput
                                    value={secondaryText}
                                    style={styles.secondaryTextInput}
                                    selectionColor={appColors.addItemGrey}
                                    multiline= {true}
                                    blurOnSubmit = {true}
                                    onChangeText={text => this.updateSecondaryInput(text)}
                                >
                                </TextInput>
                            </View>

                            <TouchableOpacity
                                onPress={() => onRemove(id, itemType)}
                                activeOpacity={0.6}
                                style={[styles.removeTouchable]}
                            >
                                <Image
                                    source={appImages.removeIcon}
                                    style={[styles.removeIcon]}
                                />
                            </TouchableOpacity>
                        </View>
                    )
                    : 
                    (
                        <View style={styles.backgroundView}>
                            <CheckBox
                                tintColors={appColors.white}
                                style={styles.checkbox}
                                disabled={false}
                                value={checkboxState}
                                onValueChange={() => this.handleCheckbox()}
                            />

                            <View style={styles.textInputsContainer}> 
                                <Text style={styles.mainTextChecked}> {mainText} </Text>
                                <Text style={styles.secondaryTextChecked} > {secondaryText} </Text>
                            </View>

                            <TouchableOpacity
                                onPress={() => onRemove(id, itemType)}
                                activeOpacity={0.6}
                                style={[styles.removeTouchable]}
                            >
                                <Image
                                    source={appImages.removeIcon}
                                    style={[styles.removeIcon]}
                                />
                            </TouchableOpacity>
                        </View>
                        
                    )
                }
            </View>
        );
    }
}
