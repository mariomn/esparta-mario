import React from 'react';
import { View, Image, TextInput } from 'react-native';
import styles from './ItemInputStyles'
import appImages from '../../Themes/appImages';
import appColors from '../../Themes/appColors';
import { TouchableOpacity } from 'react-native';

export interface Props {
    onSubmit: (main:string, secondary:string) => void,
}

export interface State {
    mainText: string,
    mainInputSubmitted: boolean,
    secondaryText: string,
    secondaryInputSubmitted: boolean,
}

export class ItemInput extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            mainText: '',
            mainInputSubmitted: false,
            secondaryText: '',
            secondaryInputSubmitted: false,
        };
    }

    updateMainInput(text:string){
        this.setState({
            mainText: text
        })
    }

    mainInputSubmitted(){
        if (this.state.mainText !== ''){
            this.setState({
                mainInputSubmitted: true
            });
        }
        this.secondaryTextInput.focus();
        this.inputsSubmitted();
    }

    updateSecondaryInput(text:string){
        this.setState({
            secondaryText: text
        })
    }

    secondaryInputSubmitted(){
        if (this.state.secondaryText !== ''){
            this.setState({
                secondaryInputSubmitted: true
            });
        }

        this.inputsSubmitted();
    }

    inputsSubmitted(){
        const { mainText, mainInputSubmitted, secondaryText, secondaryInputSubmitted } = this.state;
        const { onSubmit } = this.props;

        if(mainInputSubmitted && secondaryInputSubmitted){
            onSubmit(mainText, secondaryText);

            this.setState({
                mainText: '',
                mainInputSubmitted: false,
                secondaryText: '',
                secondaryInputSubmitted: false,
            })

            this.mainTextInput.clear();
            this.secondaryTextInput.clear();
        }
    }

    //referencia aos objetos text input para usar seus métodos
    mainTextInput: any = React.createRef();
    secondaryTextInput: any = React.createRef();

    render() {
        const { mainInputSubmitted, secondaryInputSubmitted } = this.state;

        return (
            <View style={styles.backgroundView}>
                <TouchableOpacity
                    onPress={() => this.mainTextInput.focus()}
                    activeOpacity={0.6}
                    style={[styles.plusTouchable]}
                >
                    <Image
                        source={appImages.plusIcon}
                        style={[styles.plusIcon]}
                    />
                </TouchableOpacity>
                
                <View style={styles.textInputsContainer}> 
                    <TextInput
                        ref={(ref) => {this.mainTextInput = ref}}
                        placeholder={'Adicionar nova tarefa'}
                        placeholderTextColor={appColors.addItemGrey}
                        style={styles.mainTextInput}
                        selectionColor={appColors.addItemGrey}
                        multiline= {true}
                        blurOnSubmit = {true}
                        onEndEditing= {() => this.mainInputSubmitted()}
                        onBlur= {() => this.mainInputSubmitted()}
                        onChangeText= {text => this.updateMainInput(text)}
                    >
                    </TextInput>

                    <TextInput
                        ref={(ref) => {this.secondaryTextInput = ref}}
                        placeholder ={'Adicionar descrição'}
                        placeholderTextColor={appColors.descriptionGrey}
                        style={styles.secondaryTextInput}
                        selectionColor={appColors.addItemGrey}
                        multiline= {true}
                        blurOnSubmit = {true}
                        onEndEditing= {() => this.secondaryInputSubmitted()}
                        onBlur= {() => this.secondaryInputSubmitted()}
                        onChangeText={text => this.updateSecondaryInput(text)}
                    >
                    </TextInput>
                </View>
            </View>
        );
    }
}
