import { TextStyle, ViewStyle, ImageStyle } from 'react-native';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import appColors from '../../Themes/appColors';

export default {
    backgroundView: {
        //backgroundColor: appColors.backgroundGrey,
        height: hp('10%'),
        width: wp('80%'),
        marginTop: hp('2%'),
        marginBottom: hp('3%'),
        paddingTop: hp('1%'),
        flexDirection: 'row',

        //borderColor: appColors.white,
        //borderWidth: 1
    } as ViewStyle,

    plusTouchable: {
        marginTop: hp('0.5%'),
        marginLeft: hp('1%'),

        //backgroundColor: 'red'
    } as ViewStyle,

    plusIcon: {
        height: wp('4.5%'),
        width: wp('4.5%'),
    } as ImageStyle,

    textInputsContainer: {
        width: '90%',
        marginLeft: wp('3%'),
    } as ViewStyle,

    mainTextInput: {
        color: appColors.addItemGrey,
        fontSize: wp('4%'),
        paddingTop: 0,
    } as TextStyle,

    secondaryTextInput: {
        color: appColors.descriptionGrey,
        fontSize: wp('2.5%'),
        paddingTop: 0,
    } as TextStyle,



};