import React from 'react';
import { HomeScreen } from './HomeScreen/HomeScreen';

const App = () => {
    return (
        <HomeScreen />
    );
};

export default App;
