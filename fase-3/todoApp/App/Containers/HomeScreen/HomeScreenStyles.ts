import { TextStyle, ViewStyle, ImageStyle } from 'react-native';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import appColors from '../../Themes/appColors';

export default {
    backgroundView: {
        backgroundColor: appColors.backgroundGrey,
        paddingTop: hp('10%'), 
        paddingLeft: wp('10%'),
    } as ViewStyle,

    title: {
        color: appColors.titleText,
        fontSize: wp('8%'),
    } as TextStyle,

    separationLine: {
        borderBottomColor: appColors.white,
        borderBottomWidth: 1,
        width: '85%',
        marginBottom: hp('5%'),
        opacity: 0.1
    } as ViewStyle,

    checkedTouchable: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '85%',
        marginBottom: hp('1%'),
    } as ViewStyle,

    checkedTitle: {
        color: appColors.titleText,
        fontSize: wp('5%'),
    } as TextStyle,

    down: {
        marginTop: hp('1%'),
        marginRight: wp('2%'),
        width: wp('6%'),
        height: undefined,
        aspectRatio: 93/57,
    } as ImageStyle,

    flatListView: {
        //flexDirection: 'column',
        height: hp('60%'),
    } as ViewStyle,

    bottom: {
        height: hp('30%'),
    } as ViewStyle,
};