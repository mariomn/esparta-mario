import React from 'react';
import { View, Text, Image, StatusBar, Alert, ScrollView, TouchableOpacity } from 'react-native';
import styles from './HomeScreenStyles'
import appColors from '../../Themes/appColors';
import appImages from '../../Themes/appImages';
import { ItemInput } from '../../Components/ItemInput/ItemInput';
import { Item, ItemType } from '../../Components/Item/Item';

interface TodoData {
    mainText: string,
    secondaryText: string,
    id: number,
}

export interface Props {
}

export interface State {
    uncheckedData: Array<TodoData>,
    checkedData: Array<TodoData>,
    showChecked: boolean,
}

let itemId: number;

export class HomeScreen extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            //uncheckedData: [{mainText:'Tarefa', secondaryText:'Descrição', id:-1}],
            //checkedData: [{mainText:'Tarefa', secondaryText:'Descrição', id:-2}],
            checkedData: [],
            uncheckedData: [],
            showChecked: false,
        };

        itemId = 0;
    }

    onNewItem = (mainText: string, secondaryText: string) => {
        const newData: TodoData ={
            mainText: mainText,
            secondaryText: secondaryText,
            id: this.newItemId()
        }

        const { uncheckedData } = this.state;

        uncheckedData.push(newData);

        this.setState({
            uncheckedData: uncheckedData
        });
    }

    checkItem(id:number, itemType:ItemType){
        const { uncheckedData, checkedData } = this.state;
        
        itemType === ItemType.UNCHECKED? 
        (
            uncheckedData.forEach(element => {
                if (element.id===id){
                    checkedData.push(element);
                    this.setState({ checkedData: checkedData });
                    uncheckedData.splice(uncheckedData.indexOf(element), 1);
                }
            })
        )
        :
        (
            checkedData.forEach(element => {
                if (element.id===id){
                    uncheckedData.push(element);
                    this.setState({ uncheckedData: uncheckedData });
                    checkedData.splice(checkedData.indexOf(element), 1);
                }
            })
        )
    }

    updateItem(id:number, mainText:string, secondaryText:string) {
        const { uncheckedData } = this.state;

        uncheckedData.forEach(element => {
            if (element.id===id){
                element.mainText=mainText;
                element.secondaryText=secondaryText;
            }
        })
    }

    testRemoveItem(id:number, itemType:ItemType){
        const { uncheckedData, checkedData } = this.state;

        itemType === ItemType.UNCHECKED? 
        (
            uncheckedData.forEach(element => {
                if (element.id===id){
                    this.generateRemoveAlert(element, itemType)
                }
            })
        )
        :
        (
            checkedData.forEach(element => {
                if (element.id===id){
                    this.generateRemoveAlert(element, itemType)
                }
            })
        )
    }

    generateRemoveAlert(item:TodoData, itemType:ItemType) {
        Alert.alert(
            "Remover tarefa?",
            item.mainText,
            [
                {
                    text: "Cancelar",
                    style: "destructive"
                },
                { 
                    text: "Sim",
                    onPress: () => this.removeItem(item, itemType), 
                    style: "default"
                }
            ],
            { cancelable: true }
        );
    }

    removeItem(item:TodoData, itemType:ItemType){
        const { uncheckedData, checkedData } = this.state;

        if (itemType === ItemType.UNCHECKED)
        {
            uncheckedData.splice(uncheckedData.indexOf(item), 1);
            this.setState({ uncheckedData: uncheckedData });
        }
        else{
            checkedData.splice(checkedData.indexOf(item), 1);
            this.setState({ checkedData: checkedData });
        }
    }

    toggleShowChecked(){
        const { showChecked } = this.state;
        this.setState({
            showChecked: !showChecked,
        })
    }
    
    newItemId():number {
        itemId++;
        return itemId;
    }

    render() {
        const { uncheckedData, checkedData} = this.state;

        return (
            <ScrollView style={styles.backgroundView}>
                <StatusBar translucent backgroundColor={appColors.backgroundGrey} />
                <Text style={styles.title}>Tarefas</Text>

                <View>
                    {
                       uncheckedData.map((item, i)=>
                       (
                        <Item 
                            id={item.id}
                            mainTextInitial={item.mainText}
                            secondaryTextInitial={item.secondaryText}
                            itemType={ItemType.UNCHECKED}
                            key={item.id.toString()}
                            onCheck={(id, itemType) => this.checkItem(id, itemType)}
                            onRemove={(id, ItemType) => this.testRemoveItem(id, ItemType)}
                            onSubmit={(id, mainText, secondaryText) =>  this.updateItem(id, mainText, secondaryText)}
                        />
                       )) 
                    }
                </View>

                <ItemInput
                    onSubmit={this.onNewItem}
                />

                <View style={[styles.separationLine]} />
                {
                    this.state.checkedData.length > 0 ? 
                    (
                        <View>
                            <TouchableOpacity
                                onPress={() => this.toggleShowChecked()}
                                activeOpacity={0.5}
                                style={[styles.checkedTouchable]}
                            >
                                <Text style={styles.checkedTitle}>Concluído ({this.state.checkedData.length})</Text>  
                                <Image
                                    source={this.state.showChecked ? appImages.up : appImages.down}
                                    style={[styles.down]}
                                />
                            </TouchableOpacity>
                        </View>
                    )
                    : 
                    null
                }

                {
                    this.state.showChecked ? 
                    (
                        checkedData.map((item, i)=>
                        (
                            <Item 
                                id={item.id}
                                mainTextInitial={item.mainText}
                                secondaryTextInitial={item.secondaryText}
                                itemType={ItemType.CHECKED}
                                key={item.id.toString()}
                                onCheck={(id, itemType) => this.checkItem(id, itemType)}
                                onRemove={(id, ItemType) => this.testRemoveItem(id, ItemType)}
                            /> 
                        )) 
                    )
                    :
                    null
                }

                <View style={styles.bottom}></View>
            </ScrollView>
        );
    }
}